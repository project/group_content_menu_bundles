<?php

namespace Drupal\group_content_menu_bundles\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\system\MenuInterface;

/**
 * The menu route controller.
 */
final class MenuController extends ControllerBase {

  /**
   * Builds the menu item edit page.
   *
   * @return string[]
   *   The render array.
   */
  public function overview(): array {
    return ['#markup' => 'Nothing to see here.'];
  }

  /**
   * Build the menu collections page.
   *
   * @return array
   *   The page render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function list(): array {
    $headers['name'] = $this->t('Name');
    $headers['operations'] = $this->t('Operations');

    $build['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#title' => 'test',
      '#rows' => [],
      '#empty' => $this->t('There are no menus yet.'),
    ];

    $core_menus = [];
    foreach ($this->entityTypeManager()->getStorage('menu')->loadMultiple(NULL) as $entity) {
      if ($row = $this->buildRow($entity)) {
        $core_menus[$entity->id()] = $row;
      }
    }

    $group_menus = [];
    foreach ($this->entityTypeManager()->getStorage('group_content_menu_type')->loadMultiple(NULL) as $entity) {
      if ($row = $this->buildRow($entity)) {
        $group_menus[$entity->id()] = $row;
      }
    }

    $compare_fn = static function ($a, $b) {
      return strcasecmp($a['title']['data'], $b['title']['data']);
    };
    usort($core_menus, $compare_fn);
    usort($group_menus, $compare_fn);
    $build['table']['#rows'] = array_merge($core_menus, $group_menus);
    return $build;
  }

  /**
   * Builds a table row.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The row entity.
   *
   * @return array
   *   The row data.
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = [
      'data' => $entity instanceof MenuInterface ? $entity->label() : 'Group Menu: ' . $entity->label(),
      'class' => ['menu-label'],
    ];
    $row['operations']['data'] = $this->buildOperations($entity);
    return $row;
  }

  /**
   * Build the operations for the row entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The row entity.
   *
   * @return array
   *   The operation array.
   */
  public function buildOperations(EntityInterface $entity) {
    $operations['manage-fields'] = [
      'title' => $this->t('Manage fields'),
      'weight' => 15,
      'url' => Url::fromRoute("entity.menu_link_content.field_ui_fields", [
        'bundle' => $entity->id(),
        'entity_type_id' => 'menu_link_content',
      ]),
    ];
    return [
      '#type' => 'operations',
      '#links' => $operations,
    ];
  }

}
