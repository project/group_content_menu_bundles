<?php

namespace Drupal\group_content_menu_bundles\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\group_content_menu\GroupContentMenuInterface;
use Drupal\menu_item_extras\Entity\MenuItemExtrasMenuLinkContent;

/**
 * An entity type for integrating group menus with menu_item_extras.
 */
class GroupContentMenuItemExtrasMenuLinkContent extends MenuItemExtrasMenuLinkContent {

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values): void {
    if (isset($values['menu_name']) && preg_match(sprintf('/%s(\d+)/', GroupContentMenuInterface::MENU_PREFIX), $values['menu_name'], $matches)) {
      $group_content_menu = \Drupal::entityTypeManager()
        ->getStorage('group_content_menu')
        ->load($matches[1]);
      if ($group_content_menu instanceof GroupContentMenuInterface) {
        $values['bundle'] = $group_content_menu->bundle();
      }
    }
    parent::preCreate($storage, $values);
  }

}
